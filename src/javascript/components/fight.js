import { controls } from '../../constants/controls';
// import { createElement } from '../helpers/domHelper';
//
import { updateHealthbar } from './arena';

// export?
const HP_INITIAL_VALUE = 100;

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    const firstFighter_fighter = new Fighter(firstFighter, resolve, 'left-fighter-indicator', controls.PlayerOneAttack, controls.PlayerOneBlock, controls.PlayerOneCriticalHitCombination);
    const secondFighter_fighter = new Fighter(secondFighter, resolve, 'right-fighter-indicator', controls.PlayerTwoAttack, controls.PlayerTwoBlock, controls.PlayerTwoCriticalHitCombination);
    Fighter.setRivals(firstFighter_fighter, secondFighter_fighter);
  });
}

class Fighter {
  constructor(info, resolve, healthIndicatorId, attackControl, defenseControl, criticalHitControl) {
    this.info = info;
    this.resolve = resolve;
    this.healthIndicatorId = healthIndicatorId;
    this.attackControl = attackControl;
    this.defenseControl = defenseControl;
    this.criticalHitControl = criticalHitControl;
    this.health = this.info.health;
    this.setControls();
    this.criticalHitAllowed = true;
  }

  static setRivals(figherOne, fighterTwo) {
    figherOne.rival = fighterTwo;
    fighterTwo.rival = figherOne;
  }

  keydown(e) {
    switch (e.code) {
      case this.attackControl:
        if (!this.block) {
          this.attackRival();
        }
        break;
      case this.defenseControl:
        this.block = true;
        break;
      case this.criticalHitControl[0]:
        this.keyCombinations[0] = true;
        break;
      case this.criticalHitControl[1]:
        this.keyCombinations[1] = true;
        break;
      case this.criticalHitControl[2]:
        this.keyCombinations[2] = true;
        break;
    }
    
    if (this.keyCombinations.every(Boolean)) {
      this.keyCombinations = [false, false, false];
      if (this.criticalHitAllowed) {
        this.criticalHit();
        this.criticalHitAllowed = false;
        setTimeout(() => this.criticalHitAllowed = true, 10000);
      }
    }
  }

  keyup(e) {
    switch (e.code) {
      case this.defenseControl:
        this.block = false;
        break;
      case this.criticalHitControl[0]:
        this.keyCombinations[0] = false;
        break;
      case this.criticalHitControl[1]:
        this.keyCombinations[1] = false;
        break;
      case this.criticalHitControl[2]:
        this.keyCombinations[2] = false;
        break;
    }
  }

  attackRival() {
    this.rival.damageFrom(this);
  }

  criticalHit() {
    this.rival.criticalHitFrom(this);
  }

  damageFrom(attacker) {
    this.health -= Math.min(getDamage(attacker, this), this.health);
    updateHealthbar(this.healthIndicatorId, this.health, this.info.health);
    if (this.health <= 0) {
      this.removeControls();
      attacker.removeControls();
      this.resolve(attacker.info);
    }
  }

  criticalHitFrom(attacker) {
    this.health -= Math.min(2 * attacker.info.attack, this.health);
    updateHealthbar(this.healthIndicatorId, this.health, this.info.health);
    if (this.health <= 0) {
      this.removeControls();
      attacker.removeControls();
      this.resolve(attacker.info);
    }
  }

  setControls() {
    this.keyCombinations = [false, false, false];

    document.addEventListener('keydown', this.keydown = this.keydown.bind(this));
    document.addEventListener('keyup', this.keyup = this.keyup.bind(this));
  }

  removeControls() {
    document.removeEventListener('keydown', this.keydown);
    document.removeEventListener('keyup', this.keyup);

    delete this.keyCombinations;
  }
}

export function getDamage(attacker, defender) {
  //
  return Math.max(getHitPower(attacker.info) - (defender.block ? getBlockPower(defender.info) : 0), 0);
}

export function getHitPower(fighter) {
  //
  return fighter.attack * (Math.random() + 1);
}

export function getBlockPower(fighter) {
  //
  return fighter.defense * (Math.random() + 1);
}
